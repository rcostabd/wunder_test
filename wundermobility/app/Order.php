<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model implements OrderInterface
{
    public $table = 'order';

    protected $hidden = ['created_at', 'updated_at'];

    protected $fillable = [
        'first_name',
        'last_name',
        'telephone',
        'address',
        'complement',
        'zip_code',
        'city',
        'iban'
    ];

    public $customer;
    public $ibanOwner;
    public $owner;

    /**
     * @param int $userId
     * @return $this
     */
    public function setCustomerId(int $userId): OrderInterface
    {
        $this->customer = $userId;
        return $this;
    }

    /**
     * @param string $iban
     * @return $this
     */
    public function setIbanOwner(string $iban): OrderInterface
    {
        $this->ibanOwner = $iban;
        return $this;
    }

    /**
     * @param string $owner
     * @return $this
     */
    public function setOwner(string $owner): OrderInterface
    {
        $this->owner = $owner;
        return $this;
    }

    /**
     * @return int
     */
    public function getCustomerId(): int
    {
        return $this->customer ?? $this->user_id;
    }

    /**
     * @return string
     */
    public function getIbanOwner(): string
    {
        return $this->ibanOwner ?? $this->iban;
    }

    /**
     * @return string
     */
    public function getOwner(): string
    {
        return $this->owner ?? $this->first_name . '' . $this->last_name;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'customerId' => $this->getCustomerId(),
            'iban' => $this->getIbanOwner(),
            'owner' => $this->getOwner()
        ];
    }
}
