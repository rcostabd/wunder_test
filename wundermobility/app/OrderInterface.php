<?php
namespace  App;


interface OrderInterface{
    public function setCustomerId(int $userId);
    public function setIbanOwner(string $iban);
    public function setOwner(string $owner);
    public function getCustomerId():int;
    public function getIbanOwner():string;
    public function getOwner():string;
    public function toArray():array;

}
