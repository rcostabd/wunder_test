<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Respect\Validation\Validator as v;

class TrackingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'form_id' => 'required',
            'data.firstname' => 'string|nullable',
            'data.lastname' => 'string|nullable',
            'data.telephone' => 'string|nullable',
            'data.address' => 'string|nullable',
            'data.complement' => 'string|nullable',
            'data.zip_code' => 'string|nullable',
            'data.city' => 'string|nullable',
            'data.account_owner' => 'string|nullable',
            'data.iban' => 'string|nullable',
        ];
    }
}
