<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Respect\Validation\Validator as v;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'form_id' =>'required',
            'data.firstname' => 'string',
            'data.lastname' => 'string',
            'data.telephone' => 'string',
            'data.address' => 'string',
            'data.complement' => 'string|nullable',
            'data.zip_code' => 'string',
            'data.city' => 'string',
            'data.account_owner' => 'string',
            'data.iban' => [
                function ($attribute, $value, $fail) {
                    if (v::iban()->validate($value) != true) {
                        $fail('The ' . $attribute . ' is invalid');
                    }
                    return true;
                }
            ]
        ];
    }
}
