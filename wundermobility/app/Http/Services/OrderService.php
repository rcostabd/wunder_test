<?php


namespace App\Http\Services;


use App\Exceptions\OrderException;
use App\Order;

class OrderService
{
    public $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @param array $params
     * @param int $userId
     * @return mixed
     * @throws OrderException
     */
    public function create(array $params, int $userId)
    {
        try {
            $order = $this->order->firstOrNew(
                [
                    'user_id' => $userId,
                    'is_sent' => 0
                ]);
            $order->user_id = $userId;
            $order->iban = $params['data']['iban'];
            $order->firstname = $params['data']['first_name'];
            $order->last_name = $params['data']['last_name'];
            $order->telephone = $params['data']['telephone'];
            $order->address = $params['data']['address'];
            $order->complement = $params['data']['complement'];
            $order->zip_code = $params['data']['zip_code'];
            $order->city = $params['data']['city'];
            $order->save();
        } catch (OrderException $exception) {
            throw $exception;
        }
        return $order;
    }

    /**
     * @param $id
     * @param $userId
     * @return string[]
     */
    public function getOrderById($id, $userId)
    {

        $result = $this->order->find($id);

        if (!$result) {
            return ['error' => "Order not found!"];
        }

        if ($result->user_id != $userId) {
            return ['error' => "This payment is not from your account!"];
        }
        return $result;
    }

}
