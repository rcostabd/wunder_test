<?php


namespace App\Http\Services;

use App\Tracking;
use Illuminate\Support\Facades\Log;

class TrackingService
{
    /**
     * @var Tracking
     */
    public $trackingModel;

    /**
     * TrackingService constructor.
     * @param Tracking $trackingModel
     */
    public function __construct(Tracking $trackingModel)
    {
        $this->trackingModel = $trackingModel;
    }

    /**
     * @param array $params
     * @param $userId
     * @return mixed
     * @throws \Exception
     */
    public function updateOrCreate(array $params, $userId)
    {

        try {
            $tracking = $this->trackingModel->firstOrNew(
                [
                    'form_id' => $params['form_id'],
                    'user_id' => $userId,
                    'is_finished' => 0
                ]);

            $tracking->form_id = $params['form_id'];
            $tracking->user_id = $userId;
            $tracking->step = $params['step'];
            $tracking->is_finished = $params['total_step'] == $params['step'] ? 1 : 0;
            $tracking->data = json_encode($params['data'], true);
            $tracking->save();
        } catch (\Exception $exception) {
            Log::critical($exception->getMessage());
            throw $exception;
        }
        return $tracking;
    }

    /**
     * @param array $params
     * @param int $userId
     * @return false
     * @throws \Exception
     */
    public function getTracking(array $params, int $userId)
    {

        try {
            $result = $this->trackingModel
                ->where('form_id', $params['form_id'])
                ->where('user_id', $userId)
                ->where('is_finished', '!=', 1)
                ->first();
            if(!$result){
                return ["data"=> null];
            }

            tap($result, function ($collection) {
                return $collection->data = json_decode($collection->data, true);
            });

            return $result;

        } catch (\Exception $exception) {
            Log::critical($exception->getMessage());
            throw $exception;
        }
    }
}
