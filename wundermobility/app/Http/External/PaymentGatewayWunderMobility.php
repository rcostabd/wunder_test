<?php


namespace App\Http\External;


use App\OrderInterface;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

class PaymentGatewayWunderMobility implements PaymentGatewayInterface
{
    protected $client;
    protected $order;

    /**
     * PaymentGatewayWunderMobility constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param OrderInterface $payment
     * @return $this
     */
    public function setOrder(OrderInterface $order): PaymentGatewayWunderMobility
    {
        $this->order = $order;
        return $this;
    }

    /**
     * @return array
     */
    public function send():array
    {
       $response =  $this->client->post(env("WUNDERMOBILITY_PAYMENT"),[
           RequestOptions::JSON => $this->order->toArray()
       ]);

       if($response->getStatusCode() == 200){
           return json_decode($response->getBody()->getContents(),true);
       }
    }
}
