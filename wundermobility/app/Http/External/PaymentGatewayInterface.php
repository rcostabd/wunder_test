<?php
namespace App\Http\External;


use App\OrderInterface;

interface PaymentGatewayInterface{
        public function setOrder(OrderInterface $order);
        public function send():array;
}
