<?php

namespace App\Http\Controllers;

use App\Http\External\PaymentGatewayWunderMobility;
use App\Http\Requests\OrderRequest;
use App\Http\Services\OrderService;
use App\Http\Services\TrackingService;
use App\Order;
use App\Payment;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    /**
     * @var OrderService
     */
    public $orderService;
    /**
     * @var PaymentGatewayWunderMobility
     */
    public $paymentGateway;

    /**
     * @var TrackingService
     */
    public $trackingService;

    /**
     * OrderController constructor.
     * @param OrderService $orderService
     * @param TrackingService $trackingService
     * @param PaymentGatewayWunderMobility $paymentGateway
     */
    public function __construct(
        OrderService $orderService,
        TrackingService $trackingService,
        PaymentGatewayWunderMobility $paymentGateway
    ) {
        $this->orderService = $orderService;
        $this->paymentGateway = $paymentGateway;
        $this->trackingService = $trackingService;
    }

    /**
     * @param OrderRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\OrderException
     * @throws \Exception
     */
    public function process(OrderRequest $request): \Illuminate\Http\JsonResponse
    {
        $payload = $request->all();

        $orderRegistered = $this->orderService->create($payload, Auth::id());

        $payment = $this->paymentGateway->setOrder($orderRegistered)
            ->send();

        if($payment['paymentDataId']){
            $orderRegistered->is_sent = 1;
            $orderRegistered->payment_id = $payment['paymentDataId'];
            $orderRegistered->save();
            return response()->json(['created' => true,'id'=>$orderRegistered->id], 201);
        }

        return response()->json(['created' => false], 403);
    }
}
