<?php

namespace App\Http\Controllers;

use App\Http\Requests\TrackingLastSavedRequest;
use App\Http\Requests\TrackingRequest;
use App\Http\Services\TrackingService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class TrackingController extends Controller
{
    /**
     * @var TrackingService
     */
    public $trackingService;

    /**
     * TrackingController constructor.
     * @param TrackingService $trackingService
     */
    public function __construct(TrackingService $trackingService)
    {
        $this->trackingService = $trackingService;
    }

    /**
     * @param TrackingRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function register(TrackingRequest $request): \Illuminate\Http\JsonResponse
    {
        $payload = $request->all();

        $this->trackingService->updateOrCreate($payload, Auth::id());

        return response()->json(['created' => true], 201);
    }

    /**
     * @param TrackingLastSavedRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function getTracking(TrackingLastSavedRequest $request): \Illuminate\Http\JsonResponse
    {
        $payload = $request->all();

        $result = $this->trackingService->getTracking($payload, Auth::id());

        return response()->json($result, 200);
    }
}
