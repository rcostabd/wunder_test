<?php

namespace App\Http\Controllers;

use App\Http\Services\OrderService;
use Illuminate\Support\Facades\Auth;

class FormController extends Controller
{
    /**
     * @var OrderService
     */
    public $orderService;

    /**
     * FormController constructor.
     * @param OrderService $orderService
     */
    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('form.index');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function successPage($id)
    {
        $order = $this->orderService->getOrderById($id,Auth::id());
        return view('form.success', ['order' => $order]);
    }
}
