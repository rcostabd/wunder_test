<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tracking extends Model
{
    public $table = 'tracking';

    protected $hidden = ['created_at', 'updated_at'];

    protected $fillable = [
        'form_id',
        'data',
        'user_id',
        'step',
        'is_finished'
    ];
}
