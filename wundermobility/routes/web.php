<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


Route::get('/', 'HomeController@index')->name('home');


Route::group(['middleware' => ['auth']], function() {
    Route::resource('roles','RoleController');
    Route::resource('users','UserController');
    Route::resource('roles','ProductController');

    Route::group(['prefix'=>'tracking'],function(){
        Route::put("/","TrackingController@register");
        Route::post("/","TrackingController@getTracking");
    });

    Route::group(['prefix'=>'order'],function(){
        Route::put("/","OrderController@process");
    });

    Route::get("/form","FormController@index");
    Route::get("/form/success/{id}","FormController@successPage");

    Route::group(['prefix'=>'customers'],function(){
        Route::post("/new","CostumerController@register");
        Route::get("/","CostumerController@index");
    });

});
