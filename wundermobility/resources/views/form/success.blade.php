@extends('layouts.app')

@section('content')
    @if (!isset($order['error']))
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">
                <i class="fab fa-cc-amazon-pay"></i>
                Payment success!
            </h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i> Payment Code</h5>
                    {{$order->payment_id}}
            </div>
        </div>
        <!-- /.card-body -->
    </div>
    @endif

    @if (isset($order['error']))
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">
                    <i class="fab fa-cc-amazon-pay"></i>
                    Payment Error!
                </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{$order['error']}}
                </div>
            </div>
            <!-- /.card-body -->
        </div>
    @endif
@endsection
