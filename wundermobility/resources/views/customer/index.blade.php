@extends('layouts.app')

@section('content')
    <div class="container col-sm-12">
        <table id="dtMaterialDesignExample" class="table table-striped" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th class="th-sm col-sm-5">Nome
                </th>
                <th class="th-sm col-sm-3">Data de cadastro
                </th>
                <th class="th-sm col-sm-4">Ações
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach ($customers as $customer)
            <tr>
                <td>{{$customer->customer->first_name}} {{$customer->customer->last_name}}</td>
                <td>{{$customer->created_at}}</td>
                <td class="float-right">
                    <button type="button" class="btn btn-outline-primary btn-sm">Compras</button>
                    <button type="button" class="btn btn-outline-success btn-sm">Profile</button>
                    <button type="button" class="btn btn-outline-danger btn-sm">Remover</button>
                </td>
            </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th>Nome
                </th>
                <th>Data de cadastro
                </th>
                <th>Ações
                </th>
            </tr>
            </tfoot>
        </table>
    </div>


@section('extra-script')

    <script>
        $(document).ready(function () {
            $('#dtMaterialDesignExample').DataTable();
            $('#dtMaterialDesignExample_wrapper').find('label').each(function () {
                $(this).parent().append($(this).children());
            });
            $('#dtMaterialDesignExample_wrapper .dataTables_filter').find('input').each(function () {
                const $this = $(this);
                $this.attr("placeholder", "Search");
                $this.removeClass('form-control-sm');
            });
            $('#dtMaterialDesignExample_wrapper .dataTables_length').addClass('d-flex flex-row');
            $('#dtMaterialDesignExample_wrapper .dataTables_filter').addClass('md-form');
            $('#dtMaterialDesignExample_wrapper select').removeClass(
                'custom-select custom-select-sm form-control form-control-sm');
            $('#dtMaterialDesignExample_wrapper select').addClass('mdb-select');
            $('#dtMaterialDesignExample_wrapper .mdb-select').materialSelect();
            $('#dtMaterialDesignExample_wrapper .dataTables_filter').find('label').remove();
        });

    </script>

@endsection

@endsection
