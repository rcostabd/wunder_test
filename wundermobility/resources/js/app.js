
require('./bootstrap');
window.Vue = require('vue');

import Vuex from 'vuex'

Vue.use(Vuex)

import VueFormWizard from 'vue-form-wizard'
import 'vue-form-wizard/dist/vue-form-wizard.min.css'
Vue.use(VueFormWizard)

import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue)

import VueLazyload from "vue-lazyload";

Vue.use(VueLazyload)

import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'

Vue.use(VueMaterial)

import MenuHome from './components/layout/MenuHome';
import FormOrder from './components/layout/FormOrder';

import axios from 'axios';

const store = new Vuex.Store({
    state: {
        tracking: {}
    },
    getters: {
        getTracking: state => {
            return state.tracking
        }
    }
})

axios.defaults.headers.common['X-CSRF-TOKEN'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
const app = new Vue({
    el: '#app',
    store:store,
    components: { MenuHome , FormOrder }
});
