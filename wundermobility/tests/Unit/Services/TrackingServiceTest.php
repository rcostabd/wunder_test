<?php


namespace Tests\Unit\Services;


use App\Http\Services\TrackingService;
use App\Tracking;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;

class TrackingServiceTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @var TrackingService
     */
    public $trackingService;

    public function setUp(): void
    {
        parent::setUp();

        $this->trackingService = new TrackingService(new Tracking());
    }

    public function testIfIsANewTrackingSaved(){

        $array = [
            'form_id' => 'form_1',
            'data' => [
                'first_name' => 'John',
                'last_name' => 'Senna',
                'telephone' => '0000000',
                'address' => 'street 0',
                'street' => 'street',
                'complement' => '35LPDO',
                'zip_code' => '0202-333',
                'city' => 'Neverland',
                'account_owner' => 'Peter Pan',
                'iban' => 'PT10230010202030'
            ],
            'step' => 1,
            'total_step' => 3,
            'token' => 'token',
        ];
        $userId  = 1;

        $result = $this->trackingService->updateOrCreate($array,$userId);
        $this->assertTrue(is_object($result));
    }

    public function testGetExceptionWhenDataHasNotRequiredFields(){

        $this->expectException(\Exception::class);
        $array = [
            'data' => [
                'first_name' => 'John',
                'last_name' => 'Senna',
                'telephone' => '0000000',
                'address' => 'street 0',
                'street' => 'street',
                'complement' => '35LPDO',
                'zip_code' => '0202-333',
                'city' => 'Neverland',
                'account_owner' => 'Peter Pan',
                'iban' => 'PT10230010202030'
            ],
            'step' => 1,
            'total_step' => 3,
            'token' => 'token',
        ];
        $userId  = 1;
        $staticClass = $this->getMockClass(Log::class);
        $staticClass::staticExpects($this->once())
            ->method('message');
        $this->trackingService->updateOrCreate($array,$userId);
    }
}
