<?php


namespace Tests\Feature;


use App\Http\Middleware\VerifyCsrfToken;
use App\Order;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class OrderTest extends TestCase
{
    use DatabaseMigrations;
    public $user;

    public function getUser()
    {
        $this->user = factory(User::class)->create();
    }

    public function testAnAuthenticatedUserCanSaveOrder(){
        $this->withoutExceptionHandling();
        $this->getUser();
        $this->withoutMiddleware(VerifyCsrfToken::class);

        $data =  [
            'form_id' => 'form_1',
            'data' => [
                'first_name' => 'John',
                'last_name' => 'Senna',
                'telephone' => '0000000',
                'address' => 'street 0',
                'complement' => '35LPDO',
                'zip_code' => '0202-333',
                'city' => 'Neverland',
                'account_owner' => 'Peter Pan',
                'iban' => 'DE77500105175264841328'
            ],
            'step' => 3,
            'total_step' => 3,
            'token' => 'token',
        ];

        $response = $this->actingAs($this->user)
            ->withSession(['banned' => false])
            ->putJson('/order/', $data);

        $result = Order::where('user_id',$this->user->id)->first();

        $this->assertTrue($result instanceof Order);
        $this->assertNotNull($result->payment_id);
        $response->assertStatus(201)
            ->assertJson([
                'created' => true,
            ]);
    }
}
