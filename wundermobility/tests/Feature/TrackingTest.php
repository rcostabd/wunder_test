<?php


namespace Tests\Feature;

use App\Http\Middleware\VerifyCsrfToken;
use App\Tracking;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class TrackingTest extends TestCase
{
    use DatabaseMigrations;

    public $user;

    public function getUser()
    {
        $this->user = factory(User::class)->create();
    }

    /**
     * @test
     */
    public function testAnAuthenticatedUserCanSaveTracking()
    {
        $this->withoutExceptionHandling();
        $this->getUser();
        $this->withoutMiddleware(VerifyCsrfToken::class);
        $data =  [
            'form_id' => 'form_1',
            'data' => [
                'first_name' => 'John',
                'last_name' => 'Senna',
                'telephone' => 'Senna',
                'address' => 'Senna',
                'complement' => 'Senna',
                'zip_code' => 'Senna',
                'city' => 'Senna',
                'account_owner' => 'Senna',
                'iban' => 'DE77500105175264841328'
            ],
            'step' => 1,
            'total_step' => 3,
            'token' => 'token',
        ];

        $response = $this->actingAs($this->user)
            ->withSession(['banned' => false])
            ->putJson('/tracking/', $data);

        $result = Tracking::where('form_id','form_1')->where('user_id' , $this->user->id)->first();

        $this->assertEquals(1,$result->step);

        $response->assertStatus(201)
            ->assertJson([
                'created' => true,
            ]);
    }

    /**
     * @test
     */
    public function testAnAuthenticatedUserCanUpdateTracking()
    {
        $this->withoutExceptionHandling();
        $this->getUser();
        $this->withoutMiddleware(VerifyCsrfToken::class);

        $data = [
            'form_id' => 'form_1',
            'data' => [
                'first_name' => 'John',
                'last_name' => 'Senna',
                'telephone' => '0000000',
                'address' => 'street 0',
                'complement' => '35L',
                'zip_code' => '0202-333',
                'city' => 'Neverland',
                'account_owner' => 'Peter Pan',
                'iban' => 'DE77500105175264841328'
            ],
            'step' => 1,
            'total_step' => 3,
            'token' => 'token',
        ];

        $this->actingAs($this->user)
            ->withSession(['banned' => false])
            ->putJson('/tracking/', $data );

        $earlyResult = Tracking::where('form_id','form_1')->where('user_id' , $this->user->id)->first();

        $data = [
            'form_id' => 'form_1',
            'data' => [
                'first_name' => 'John',
                'last_name' => 'Senna',
                'telephone' => '0000000',
                'address' => 'street 0',
                'complement' => '35LPDO',
                'zip_code' => '0202-333',
                'city' => 'Neverland',
                'account_owner' => 'Peter Pan',
                'iban' => 'DE77500105175264841328'
            ],
            'step' => 2,
            'total_step' => 3,
            'token' => 'token',
        ];

        $response = $this->actingAs($this->user)
            ->withSession(['banned' => false])
            ->putJson('/tracking/', $data );

        $result = Tracking::where('form_id','form_1')->where('user_id' , $this->user->id)->first();

        $expected = json_decode($result->data,true);

        $this->assertEquals($expected,$data['data']);

        $this->assertEquals(2,$result->step);
        $this->assertEquals(1,$earlyResult->step);

        $response->assertStatus(201)
            ->assertJson([
                'created' => true,
            ]);
    }

    /**
     * @test
     */
    public function testIfCreateNewTrackingRegisterWhenPreviousIsFinished()
    {
        $this->withoutExceptionHandling();
        $this->getUser();
        $this->withoutMiddleware(VerifyCsrfToken::class);

        $data = [
            'form_id' => 'form_1',
            'data' => [
                'first_name' => 'John',
                'last_name' => 'Senna',
                'telephone' => '0000000',
                'address' => 'street 0333',
                'complement' => '35L',
                'zip_code' => '0202-333',
                'city' => 'Neverland',
                'account_owner' => 'Peter Pan',
                'iban' => 'DE77500105175264841328'
            ],
            'step' => 3,
            'total_step' => 3,
            'token' => 'token',
        ];

        $this->actingAs($this->user)
            ->withSession(['banned' => false])
            ->putJson('/tracking/', $data );

        $data = [
            'form_id' => 'form_1',
            'data' => [
                'first_name' => 'John',
                'last_name' => 'Senna',
                'telephone' => '0000000',
                'address' => 'street 0',
                'complement' => '35LPDO',
                'zip_code' => '0202-333',
                'city' => 'Neverland',
                'account_owner' => 'Peter Pan',
                'iban' => 'DE77500105175264841328'
            ],
            'step' => 1,
            'total_step' => 3,
            'token' => 'token',
        ];

        $response = $this->actingAs($this->user)
            ->withSession(['banned' => false])
            ->putJson('/tracking/', $data );

        $result = Tracking::where('form_id','form_1')->where('user_id' , $this->user->id)->get();

        $this->assertEquals(2,$result->count());

        $response->assertStatus(201)
            ->assertJson([
                'created' => true,
            ]);
    }

    public function testGetTrackingAreReturnTheLastObjectSaved(){
        $this->withoutExceptionHandling();
        $this->getUser();
        $this->withoutMiddleware(VerifyCsrfToken::class);

        $data = [
            'form_id' => 'form_1',
            'data' => [
                'first_name' => 'John',
                'last_name' => 'Senna',
                'telephone' => '0000000',
                'address' => 'street 0',
                'complement' => '35L',
                'zip_code' => '0202-333',
                'city' => 'Neverland',
                'account_owner' => 'Peter Pan',
                'iban' => 'DE77500105175264841328'
            ],
            'step' => 1,
            'total_step' => 3,
            'token' => 'token',
        ];

        $this->actingAs($this->user)
            ->withSession(['banned' => false])
            ->putJson('/tracking/', $data );

        $response = $this->actingAs($this->user)
            ->withSession(['banned' => false])
            ->postJson('/tracking/', [
                'form_id'=>'form_1'
            ] );

        $response->assertStatus(200)
            ->assertJson( [
                "id" => "1",
                "form_id" => "form_1",
                'data' => [
                    'first_name' => 'John',
                    'last_name' => 'Senna',
                    'telephone' => '0000000',
                    'address' => 'street 0',
                    'complement' => '35L',
                    'zip_code' => '0202-333',
                    'city' => 'Neverland',
                    'account_owner' => 'Peter Pan',
                    'iban' => 'DE77500105175264841328'
                ],
                "user_id" => "1",
                "step" => "1",
                "is_finished" => "0"
            ]);
    }
}
